<section>
    <header><h1 class="page-header">Register</h1></header>
    <div class="page-content">
        <form class="register-form" method="post">
            <div class="input-container text">
                <label>
                    <span class="form-label">Email:</span>
                    <input type="text" name="email" class="form-input" 
                    <?php if (empty($errors['email'])): ;?>
                        
                        "value='" <?php echo $_POST["email"];?> "'"
                    />
                    <?php endif; ?> 
                </label>
            </div>
            <?php if (!empty($errors['email'])):?>
                <span><?php echo $errors['email'] ?></span>
            <?php endif; ?>    
            <div class="input-container text">
                <label>
                    <span class="form-label">Password:</span>
                    <input type="password" name="password" class="form-input" />
                </label>
            </div>
            <?php if (!empty($errors['password'])):?>
                <span><?php echo $errors['password'] ?></span>
            <?php endif; ?>  

            <div class="input-container text">
                <label>
                    <span class="form-label">Confirm Password:</span>
                    <input type="password" name="confirm_password" class="form-input" />
                                    </label>
            </div>
            <?php if (!empty($errors['password'])):?>
                <span><?php echo $errors['password'] ?></span>
            <?php endif; ?>  
            <div>
                <input type="submit" name="action" value="Register" class="form-submit" />
            </div>
        </form>
</section>