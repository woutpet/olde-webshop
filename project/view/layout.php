<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>AirdropSupplies</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<div id="page">
		<header class="header-logo"><h1><a href="index.php"><span>Airdrop Supplies</span></a></h1></header>
		<div class="right-corner">
			<section class="login-container">
		        <?php if (empty($_SESSION['user'])): ?>
		                <header><h1>Login/<a href="index.php?page=register">register</a></h1></header>
		                <form class="login-form" method="post" action="index.php?page=login">
		                    <input type="hidden" name="action" value="login" />
		                    <div class="input-container text">
		                        <label>
		                            <span class="form-label hidden">Email:</span>
		                            <input type="text" name="email" placeholder="email" class="form-input" />
		                        </label>
		                    </div>
		                    <div class="input-container text">
		                        <label>
		                            <span class="form-label hidden">Password:</span>
		                            <input type="password" name="password" placeholder="password" class="form-input" />
		                        </label>
		                    </div>
		                    <div class="input-container submit">
		                        <button type="submit" class="form-submit">Login</button>
		                    </div>
		                </form>
		        <?php else: ?>
		            <p><a href="index.php?page=account"><?php echo $_SESSION['user']['email'] ?></a> - <a href="index.php?page=logout" class="logout-button">Logout</a></p>
		        <?php endif; ?>
	        </section>
	        <section>
				<ul>
					<li>
						<a href="index.php?page=cart">View Cart (<?php 

							if (empty($_SESSION["cart"])) {
								echo "0 items";
							}else{
								echo sizeof($_SESSION["cart"]) . " item";
								if (sizeof($_SESSION["cart"]) > 1){
									echo "s";
								}
							}

						?>)</a>
					</li>
				</ul>
				<form id="searchForm" method="get" action="index.php?action=search" class="form-horizontal">
					<div class="form-group">
						<div class="col-sm-10">
							<input type="search" name="q" class="form-control" placeholder="Search here" value="<?php
							if(!empty($_GET['q'])) {
								echo $_GET['q'];
							}
							?>" autocomplete="off" />
						</div>
						<div class="col-sm-2">
							<button type="submit" class="btn btn-default btn-block"><span class="glyphicon glyphicon-search"></span> Search</button>
						</div>
					</div>
				</form>
			</section>

		</div>
		<?php
			if(!empty($error)) {
				echo '<div class="error box">' . $error . '</div>';
			}
			if(!empty($info)) {
				echo '<div class="info box">' . $info . '</div>';
			}
		?>
		<?php echo $content; ?>
	</div>



	<script src="js/jquery.1.11.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/app.js"></script>

</body>
</html>