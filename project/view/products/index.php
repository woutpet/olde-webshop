<section>
	<header class="hidden"><h1>Products</h1></header>
	<ul class="product-grid">
		<?php if(!empty($products)): ?>
		<?php foreach($products as $product): ?>
			<li>
				<a href="index.php?page=detail&amp;id=<?php echo $product['id'];?>">
					<div class='image-holder'>
						<img src="img/<?php echo $product['image'];?>" alt="<?php echo $product['title'];?>" />
					</div>
					<h2><?php echo $product['title'];?></h2>
					<p>&euro; <?php echo $product['price'];?></p>
				</a>
			</li>
		<?php endforeach; ?>
		<?php else: ?>
			<li>No results</li>
		<?php endif; ?>
	</ul>
</section>