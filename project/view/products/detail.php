<section>
	<div id="product-image">
		<?php echo '<img src="img/' . $product['image'] . '" alt="' . $product['title'] . '" />'; ?>
	</div>
	<div id="product-information">
		<header><h1 class="product-title"><?php echo $product['title'];?></h1></header>
		<div id="product-pricing">
			<p class="price">&euro; <?php echo $product['price'];?></p>
			<form method="post" action="index.php?page=add-to-cart&amp;id=<?php echo $product['id'];?>">
				<input class="btn" type="submit" name="add" id="add-to-cart" value="Add to Cart">
			</form>
		</div>
		<div id="product-description"><?php echo $product['description'];?></div>
		<section class="more-images clearfix">
			<header><h1>More Images</h1></header>
			<ul>
				
			</ul>
		</section>
	</div>

	<section class="review">
		<header>
			<h3>Reviews</h3>
		</header>
		<?php if(empty($reviews)): ?>
			<p>No reviews yet.</p>
		<?php else: ?>
			<ul>
				<?php foreach($reviews as $review): ?>
					<li><h4><?php echo $review['title'] ?></h4><?php echo $review['content'] ?> by <?php echo $review['email'] ?></li>
				<?php endforeach; ?>
			</ul>
		<?php endif; ?>
	</section>

	<?php if(!empty($_SESSION['user'])): ?>
		<?php if($hasBought): ?>
		<form class="addReview" method="POST">
			<label class="reviewTitle">Review title:
				<input type="text" name="title" />
				<?php if(!empty($errors['title'])): ?>
					<p><?php echo $errors['title'] ?></p>
				<?php endif; ?>	
			</label>
			
			<label class="reviewItself">Review:
				<textarea name="content"></textarea>
				<?php if(!empty($errors['content'])): ?>
					<p><?php echo $errors['content'] ?></p>
				<?php endif; ?>	
			</label>

			<input type="submit" name="Action" value="Add Review" />

		</form>
		<?php else: ?>
			<p>You have not bought this product yet!</p>
		<?php endif; ?>	
	<?php else: ?>
		<p>Please <a href="index.php?page=register">register</a> or login to place a comment</p>
	<?php endif; ?>	

</section>