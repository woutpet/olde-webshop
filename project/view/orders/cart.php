	
	<div id="content" class="cart">
		<form action="index.php?page=cart" method="post" id="cartform">
			<table class='cart'>
				<thead>
					<tr>
						<th class='product-description' colspan='2'></th>
						<th class='price'>Price</th>
						<th class='quantity'>Quantity</th>
						<th class='remove-item'></th>
						<th class='total'>Total</th>
					</tr>
				</thead>

				<tbody>

					<!-- template voor 1 cart item -->
					<?php 

						$total = 0;

					?>
					<?php foreach ($items as $item):?>

						<?php 
							$itemTotal = $item["product"]["price"] * $item["amount"]; 
							$total += $itemTotal;
						?>

						<tr class="item">
						<td class='product-image'>
							<a <?php echo "href='index.php?page=detail&amp;id=" . $item["product"]["id"] . "'"?>>
								<img src=<?php echo "'img/" . $item["product"]["image"] . "'"; ?>  alt=<?php echo "'" . $item["product"]["title"] . "'"?> />
							</a>
						</td>
						<td class='product-description'><?php echo $item["product"]["title"] ?></td>
						<td class='price'>&euro;<?php echo $item["product"]["price"] ?></td>
						<td class='quantity'> <input class="text quantity" type="text" size="4" name=<?php echo "'amount[1]'"; ?> value=<?php echo "'" . $item["amount"] . "'"; ?> class="replace" /> </td>
						<td class='remove-item'><a class="btn remove-from-cart" href=<?php echo "'index.php?page=cart&amp;action=change&amp;id=" . $item["product"]["id"] . "&amp;amount=0'" ?>>Remove</a></td>
						<td class='total'>&euro;<?php echo $itemTotal ?></td>
					</tr>

					<?php endforeach; ?>
					<!-- end template voor 1 cart item -->
						
					
				</tbody>
				</table>
				<div class='column two'>
					<p class='order-total'><span>total:</span> <?php echo $total; ?></p>
					<p><input type="submit" id="update-cart" class="btn" name="update" value="Update Cart" /></p>
					
					<p><input class="btn-reversed btn" type="submit" id="checkout" name="checkout" value="Checkout" /></p>
					
				</div> 

				

			
			<div class='column one'>
				<div id="checkout-addnote">
					<label for="note" class='note'>Special Instructions / Gift Note:</label>
					<textarea id="note" name="note"></textarea>
					<div class='gift-receipt'>
						<input id="gift-receipt" type="checkbox" name="attributes[gift-receipt]" value="yes"  /><label for="gift-receipt" class='gift-r'>Please include a gift receipt with the note above.</label>
					 </div>
				</div>
			</div>	
		</form>
	</div>