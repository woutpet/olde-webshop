-- phpMyAdmin SQL Dump
-- version 4.2.5
-- http://www.phpmyadmin.net
--
-- Machine: localhost:8889
-- Gegenereerd op: 08 mei 2015 om 14:01
-- Serverversie: 5.5.38
-- PHP-versie: 5.5.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databank: `yow`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `yow_orders`
--

CREATE TABLE `yow_orders` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=46 ;

--
-- Gegevens worden geëxporteerd voor tabel `yow_orders`
--

INSERT INTO `yow_orders` (`id`, `user_id`, `date`) VALUES
(1, 1, '2015-05-04'),
(2, 1, '2015-05-04'),
(3, 1, '2015-05-04'),
(4, 1, '2015-05-04'),
(5, 1, '2015-05-04'),
(6, 1, '2015-05-04'),
(7, 1, '2015-05-04'),
(8, 1, '2015-05-04'),
(9, 1, '2015-05-04'),
(10, 1, '2015-05-04'),
(11, 1, '2015-05-04'),
(12, 1, '2015-05-04'),
(13, 1, '2015-05-04'),
(14, 1, '2015-05-04'),
(15, 1, '2015-05-04'),
(16, 1, '2015-05-04'),
(17, 1, '2015-05-04'),
(18, 1, '2015-05-04'),
(19, 1, '2015-05-04'),
(20, 1, '2015-05-04'),
(21, 1, '2015-05-04'),
(22, 1, '2015-05-04'),
(23, 1, '2015-05-04'),
(24, 1, '2015-05-07'),
(25, 1, '2015-05-07'),
(26, 1, '2015-05-07'),
(27, 1, '2015-05-07'),
(28, 1, '2015-05-07'),
(29, 1, '2015-05-07'),
(30, 1, '2015-05-07'),
(31, 1, '2015-05-07'),
(32, 1, '2015-05-07'),
(33, 1, '2015-05-07'),
(34, 1, '2015-05-07'),
(35, 1, '2015-05-07'),
(36, 1, '2015-05-07'),
(37, 1, '2015-05-07'),
(38, 1, '2015-05-07'),
(39, 1, '2015-05-08'),
(40, 1, '2015-05-08'),
(41, 1, '2015-05-08'),
(42, 1, '2015-05-08'),
(43, 1, '2015-05-08'),
(44, 1, '2015-05-08'),
(45, 1, '2015-05-08');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `yow_orders_products`
--

CREATE TABLE `yow_orders_products` (
`id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- Gegevens worden geëxporteerd voor tabel `yow_orders_products`
--

INSERT INTO `yow_orders_products` (`id`, `order_id`, `product_id`, `amount`) VALUES
(10, 38, 3, 1),
(11, 38, 1, 1),
(12, 38, 2, 1),
(13, 39, 1, 1),
(14, 44, 1, 1),
(15, 45, 1, 8);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `yow_products`
--

CREATE TABLE `yow_products` (
`id` int(11) NOT NULL,
  `title` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `sold` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Gegevens worden geëxporteerd voor tabel `yow_products`
--

INSERT INTO `yow_products` (`id`, `title`, `image`, `description`, `price`, `stock`, `sold`) VALUES
(1, 'Medkit', '1.png', 'For bigger wounds.', 10, 90, 9),
(2, 'Ammo', '2.png', 'To fill up your guns.', 20, 200, 9),
(3, 'Pistol', '3.png', 'Perfect to defend from small threats.', 50, 50, 1),
(4, 'Machine gun', '4.png', 'For the bigger threats.', 150, 200, 3),
(5, 'RPG', '5.png', 'To destroy multiple threats at once.', 250, 55, 987654),
(6, 'Food', '6.png', 'A food supply.', 5, 2000, 3472),
(7, 'Medicine', '7.png', 'For small wounds.', 5, 150, 210),
(8, 'Water', '8.png', 'A water supply.', 2, 5609, 4356),
(9, 'Grenades', '9.png', 'For cool explosions.', 10, 34, 90),
(10, 'Air support.', '10.png', 'Kill an entire army at once!', 500, 20, 3);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `yow_reviews`
--

CREATE TABLE `yow_reviews` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `product_id` int(11) NOT NULL,
  `creation_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `yow_users`
--

CREATE TABLE `yow_users` (
`id` int(11) NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `creation_date` date NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Gegevens worden geëxporteerd voor tabel `yow_users`
--

INSERT INTO `yow_users` (`id`, `password`, `email`, `creation_date`) VALUES
(1, '$2y$12$PvEXww67ZFZfFPb8CKEuiuvvyUzK10dtz96APe4ZXvGS/SXRiKfKy', 'wouterpetrens@gmail.com', '2015-05-01');

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `yow_orders`
--
ALTER TABLE `yow_orders`
 ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `yow_orders_products`
--
ALTER TABLE `yow_orders_products`
 ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `yow_products`
--
ALTER TABLE `yow_products`
 ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `yow_reviews`
--
ALTER TABLE `yow_reviews`
 ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `yow_users`
--
ALTER TABLE `yow_users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `yow_orders`
--
ALTER TABLE `yow_orders`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT voor een tabel `yow_orders_products`
--
ALTER TABLE `yow_orders_products`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT voor een tabel `yow_products`
--
ALTER TABLE `yow_products`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT voor een tabel `yow_reviews`
--
ALTER TABLE `yow_reviews`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `yow_users`
--
ALTER TABLE `yow_users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
