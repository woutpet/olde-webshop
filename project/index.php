<?php
session_start();

// $_SESSION["cart"][3] = 2;
// $_SESSION["cart"][4] = 1;
// $_SESSION["cart"][12] = 5;



define('DS', DIRECTORY_SEPARATOR);
define('WWW_ROOT', __DIR__ . DS);

$routes = array(
    'home' => array(
        'controller' => 'Products',
        'action' => 'index'
    ),
    'detail' => array(
        'controller' => 'Products',
        'action' => 'detail'
    ),
    'cart' => array(
        'controller' => 'Orders',
        'action' => 'cart'
    ),
    'add-to-cart' => array(
        'controller' => 'Orders',
        'action' => 'add_product'
    ),
    'register' => array(
        'controller' => 'Users',
        'action' => 'register'
    ),
    'login' => array(
        'controller' => 'Users',
        'action' => 'login'
    ),
    'logout' => array(
        'controller' => 'Users',
        'action' => 'logout'
    ),
    'account' => array(
        'controller' => 'Users',
        'action' => 'account'
    )
);

if(empty($_GET['page'])) {
    $_GET['page'] = 'home';
}
if(empty($routes[$_GET['page']])) {
    header('Location: index.php');
    exit();
}

$route = $routes[$_GET['page']];
$controllerName = $route['controller'] . 'Controller';

require_once WWW_ROOT . 'controller' . DS . $controllerName . ".php";

$controllerObj = new $controllerName();
$controllerObj->route = $route;
$controllerObj->filter();
$controllerObj->render();