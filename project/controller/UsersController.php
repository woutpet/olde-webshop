<?php

require_once WWW_ROOT . 'controller' . DS . 'Controller.php';

require_once WWW_ROOT . 'dao' . DS . 'UserDAO.php';
require_once WWW_ROOT . 'dao' . DS . 'OrderDAO.php';

require_once WWW_ROOT . 'phpass' . DS . 'Phpass.php';


class UsersController extends Controller {

	private $OrderDAO;

	function __construct() {
		$this->userDAO = new UserDAO();
		$this->orderDAO = new OrderDAO();
	}

	public function account(){

		if(!empty($_SESSION['user'])){

			$orders = $this->orderDAO->selectByUserId($_SESSION['user']['id']);

			$this->set("orders", $orders);

			
		}

	}

	public function register() {
		if(!empty($_POST)){

			$errors = array();

			//Eerst checken of alles wel goed is ingevuld.

			if(empty($_POST['email'])){
				$errors['email'] = "Please fill in your email";
			}else{
				//Checken of het email al bestaat in de database als er een email wordt ingevuld.
				$emailExist = $this->userDAO->selectByEmail($_POST['email']);
				if(!empty($emailExist)){
					$errors['email'] = "Email already used!";
				}
			}

			if(empty($_POST['password'])){
				$errors['password'] = "Please fill in a password";
			}else{
				if($_POST['password'] != $_POST['confirm_password']){
					$errors['password'] = "Passwords do not match";
				}
			}

			//We checken eerst alles. Als er geen fouten zijn doe dan voort.
			if(empty($errors)){
				$hasher = new \Phpass\Hash;
				$userData = array();
				$userData['email'] = $_POST['email'];
				$userData['password'] = $hasher->hashPassword($_POST['password']);
				$userData['creation_date'] = date('Y-m-d H:i:s');
				$insertedUser = $this->userDAO->insert($userData);
				if(!empty($insertedUser)){
					$_SESSION['info'] = "Registration succesful";
					$_SESSION['user'] = $insertedUser;
					$this->redirect('index.php');
				}
			}

			//Als het gelukt is dan is de pagina al geredirect dus komt hij hier niet meer.
			//Als hij hier wel nog komt is het het teken dat de registratie mislukt is.

			$_SESSION['errors'] = "Registration failed";
			$this->set('errors', $errors);
		}
	}

	public function logout(){
		if(!empty($_SESSION['user'])){
			unset($_SESSION['user']);
		}
		$this->redirect('index.php');
	}

	public function login(){
		if(!empty($_POST)){
			if(!empty($_POST['email']) && !empty($_POST['password'])){
				//Bestaat er gebruiker met dat wachtwoord? zoja haal deze op.
				$existing_user = $this->userDAO->selectByEmail($_POST['email']);
				if(!empty($existing_user)){
					$hasher = new \Phpass\Hash;
					$password_check = $hasher->checkpassword($_POST['password'], $existing_user['password']);
					if($password_check){
						$_SESSION['user'] = $existing_user;
						$_SESSION['info'] = "You are logged in";
					}else{
						$_SESSION['error'] = "Unknown password";
					}
				}else{
					$_SESSION['error'] = "Unknown email";
				}
			}else{
				$_SESSION['error'] = "Please fill in a password/email";
			}
		}
		$this->redirect('index.php');
	}
}