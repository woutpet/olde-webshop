<?php

require_once WWW_ROOT . 'controller' . DS . 'Controller.php';

require_once WWW_ROOT . 'dao' . DS . 'ProductDAO.php';
require_once WWW_ROOT . 'dao' . DS . 'OrderDAO.php';
require_once WWW_ROOT . 'dao' . DS . 'OrderProductDAO.php';

class OrdersController extends Controller {

	private $productDAO;
	private $orderDAO;
	private $orderProductDAO;

	function __construct() {
		$this->productDAO = new ProductDAO();
		$this->orderDAO = new OrderDAO();
		$this->orderProductDAO = new OrderProductDAO();
	}

	public function cart() {
		if(!isset($_SESSION['cart'])) {
			$_SESSION['cart'] = array();
		}
		if(!empty($_POST)) {
			if(!empty($_POST['checkout'])) {
				$this->_handleCheckout();
			} else if(!empty($_POST['update'])) {
				$this->_handleUpdateCart();
			}
		}
		if(!empty($_GET['action'])) {
			if($_GET['action'] == 'change') {
				$this->_handleChange();
			}
		}
		
		$items = array();
		foreach($_SESSION['cart'] as $productId => $amount) {
			if ($product = $this->productDAO->selectById($productId)) {
				if ($amount > 0) {
					$items[] = array('product' => $product, 'amount' => $amount);
				}
			}
		}
		$this->set('items', $items);
	}

	public function add_product() {
		if (!empty($_GET["id"])) {
			if ($product = $this->productDAO->selectById($_GET["id"])) {
				if (!isset($_SESSION["cart"])) {
					$_SESSION["cart"] = array();
				}
				if (isset($_SESSION["cart"][$_GET["id"]])) {
					$_SESSION["cart"][$_GET["id"]]++;
				}else{
					$_SESSION["cart"][$_GET["id"]] = 1;
				}
				$_SESSION["info"] = "product added to cart";
				$this->redirect("index.php?page=detail&id=" . $_GET["id"]);	
			}
		}
		$this->redirect("index.php");		
	}

	private function _handleCheckout() {
		if(!empty($_SESSION['user'])){

			$data = array('user_id' => $_SESSION['user']['id'],
							'date' => date('Y-m-d H:i:s')
						);

			$orderId = $this->orderDAO->insert($data);

			$data = array();

			foreach ($_SESSION["cart"] as $productId => $amount) {

				//$product = $this->productDAO->selectById($productId);

				$data[] = array(
					'product_id' => $productId,
					'order_id' => $orderId['id'],
					'amount' => $amount
				);

				$productToUpdate = $this->productDAO->selectById($productId);

				$productToUpdate["stock"] -= $amount;
				$productToUpdate["sold"] += $amount;

				$dataToUpdate = array(
					'id' => $productId,
					'stock' => $productToUpdate["stock"],
					'sold' => $productToUpdate["sold"] 
				);

				$this->productDAO->update($dataToUpdate);
				
			}

			if ($this->orderProductDAO->insertMultiple($data)) {
				$_SESSION["info"] = "Thanks for your purchase, it will be dropped soon!";
				unset($_SESSION["cart"]);
				$this->redirect('index.php');
			}else{
				$_SESSION["error"] = "Could not process order!";

			}
			
		}
		$this->redirect('index.php');
	}

	private function _handleUpdateCart() {
		foreach ($_POST["amount"] as $productId => $amount) {
			$_SESSION["cart"][$productId] = $amount;
			if ($amount == 0) {
				unset($_SESSION["cart"][$productId]);
			}
		}
	}

	private function _handleChange() {
		if (!empty($_GET["id"]) && isset($_GET["amount"]) && isset($_SESSION["cart"][$_GET["id"]])) {
			$_SESSION["cart"][$_GET["id"]] = $_GET["amount"];
			if ($_GET["amount"] == 0){
				unset($_SESSION["cart"][$_GET["id"]]);
				$this->redirect("index.php?page=cart");
			}
		}
	}

}