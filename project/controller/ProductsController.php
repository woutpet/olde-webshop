<?php

require_once WWW_ROOT . 'controller' . DS . 'Controller.php';

require_once WWW_ROOT . 'dao' . DS . 'ProductDAO.php';
require_once WWW_ROOT . 'dao' . DS . 'ReviewDAO.php';

require_once WWW_ROOT . 'dao' . DS . 'OrderDAO.php';
// require_once WWW_ROOT . 'dao' . DS . 'ProductImageDAO.php';

class ProductsController extends Controller {

	private $productDAO;
	private $reviewDAO;

	private $orderDAO;


	function __construct() {
		$this->productDAO = new ProductDAO();
		$this->reviewDAO = new ReviewDAO();

		$this->orderDAO = new OrderDAO();		
		// $this->productImageDAO = new ProductImageDAO();
	}

	public function index() {
		if(!empty($_GET['q'])) {
			$this->set('products', $this->productDAO->searchProducts($_GET['q']));
		}else{
			$products = $this->productDAO->selectAll();
			$this->set('products', $products);
		}
		
	}

	public function detail() {
		if(empty($_GET['id']) || !$product = $this->productDAO->selectById($_GET['id'])) {
			$_SESSION['error'] = 'Invalid Product';
			$this->redirect('index.php');
		}

		$errors = array();
		if(!empty($_POST)){

			if(empty($_POST['title'])){
				$errors['title'] = "Please fill in a title";
			}
			if(empty($_POST['content'])){
				$errors['content'] = "Please fill in a review";
			}
		}
		
		if(empty($errors)){
			if(!empty($_POST)){
				if($_POST['Action'] == "Add Review"){
					$this->handleReview($product);
				}
			}
		}

		$this->set('errors', $errors);


		// $productImages = $this->productImageDAO->selectByProductId($product['id']);

		// $largeImage = $productImages[0];
		// if(!empty($_GET['image']) && $productImage = $this->productImageDAO->selectById($_GET['image'])) {
		// 	$largeImage = $productImage;
		// }

		// $this->set('product', $product);
		// $this->set('productImages', $productImages);
		// $this->set('largeImage', $largeImage);

		$this->set('product', $product);
		//REVIEWS OPHALEN
		$reviews = $this->reviewDAO->selectByProductId($product['id']);
		$this->set('reviews', $reviews);

		$allOrders = $this->orderDAO->selectByUserId($_SESSION["user"]["id"]);

		$hasBought = false;

		foreach ($allOrders as $order) {
			if ($order["product_id"] == $product["id"]) {
				$hasBought = true;
			}
		}

		$this->set('hasBought', $hasBought);

	}

	private function handleReview($product){
		$reviewData = array();
		$reviewData['user_id'] = $_SESSION['user']['id'];
		$reviewData['title'] = $_POST['title'];
		$reviewData['content'] = $_POST['content'];
		$reviewData['product_id'] = $product['id'];
		$reviewData['creation_date'] = date('Y-m-d H:i:s');		

		$insertedReview = $this->reviewDAO->insert($reviewData);

		if(!empty($insertedReview)){
			$_SESSION['info'] = "Review added";
			$this->redirect("index.php?page=detail&id=" . $product['id']);
		}else{
			$_SESSION['error'] = "Review failed to add";
			$this->set('errors', $this->ReviewDAO->getValidationErrors($data));
		}	
	}

}