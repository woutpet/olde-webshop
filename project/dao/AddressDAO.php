<?php
require_once __DIR__ . '/DAO.php';
class AddressDAO extends DAO {

	public function selectAll() {
		$sql = "SELECT * FROM `yow_address`";
		$stmt = $this->pdo->prepare($sql);
		$stmt->execute();
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}

	public function selectById($id) {
		$sql = "SELECT * FROM `yow_address` WHERE `id` = :id";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(':id', $id);
		$stmt->execute();
		return $stmt->fetch(PDO::FETCH_ASSOC);
	}

	public function selectByUserId($userId) {
		$sql = "SELECT * FROM `yow_address` WHERE `user_id` = :user_id";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(':user_id', $userId);
		$stmt->execute();
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}

	public function insert($data) {
		$errors = $this->getValidationErrors($data);
		if(empty($errors)) {
			$sql = "INSERT INTO `yow_address` (`country`, `city`, `street`, `number`, `user_id`) VALUES (:country, :city, :street, :number, :user_id)";
			$stmt = $this->pdo->prepare($sql);
			$stmt->bindValue(':country', $data['country']);
			$stmt->bindValue(':city', $data['city']);
			$stmt->bindValue(':street', $data['street']);
			$stmt->bindValue(':number', $data['number']);
			$stmt->bindValue(':user_id', $data['user_id']);
			if($stmt->execute()) {
				$insertedId = $this->pdo->lastInsertId();
				return $this->selectById($insertedId);
			}
		}
		return false;
	}

	public function delete($id) {
		$sql = "DELETE FROM `yow_address` WHERE `id` = :id";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(':id', $id);
		return $stmt->execute();
	}

	public function getValidationErrors($data) {
		$errors = array();
		if(!isset($data['country'])) {
			$errors['country'] = "Please fill in a country";
		}
		if(!isset($data['city'])) {
			$errors['city'] = "Please fill in a city";
		}
		if(empty($data['street'])) {
			$errors['street'] = "Please fill in a street";
		}
		if(!isset($data['number'])) {
			$errors['number'] = "Please fill in a number";
		}
		if(!isset($data['user_id'])) {
			$errors['user_id'] = "Please fill in a user id";
		}
		return $errors;
	}

}