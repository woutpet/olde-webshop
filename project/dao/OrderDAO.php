<?php
require_once __DIR__ . '/DAO.php';
class OrderDAO extends DAO {

	public function selectAll() {
		$sql = "SELECT * FROM `yow_orders`";
		$stmt = $this->pdo->prepare($sql);
		$stmt->execute();
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}

	public function selectById($id) {
		$sql = "SELECT * FROM `yow_orders` WHERE `id` = :id";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(':id', $id);
		$stmt->execute();
		return $stmt->fetch(PDO::FETCH_ASSOC);
	}

	public function selectByUserId($id){
		$sql = "SELECT *
				FROM `yow_orders_products` 
				INNER JOIN `yow_orders` ON `yow_orders_products`.`order_id` = `yow_orders`.`id`
				INNER JOIN `yow_products` ON `yow_orders_products`.`product_id` = `yow_products`.`id`
				WHERE `user_id` = :id";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(':id', $id);
		$stmt->execute();
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}

	// public function selectById($postId) {
	// 	$sql = "SELECT * FROM `yow_orders` WHERE `review_id` = :review_id";
	// 	$stmt = $this->pdo->prepare($sql);
	// 	$stmt->bindValue(':review_id', $postId);
	// 	$stmt->execute();
	// 	return $stmt->fetchAll(PDO::FETCH_ASSOC);
	// }

	public function insert($data) {
		$errors = $this->getValidationErrors($data);
		if(empty($errors)) {
			$sql = "INSERT INTO `yow_orders` (`user_id`, `date`) VALUES (:user_id, :date)";
			$stmt = $this->pdo->prepare($sql);
			$stmt->bindValue(':user_id', $data['user_id']);
			$stmt->bindValue(':date', $data['date']);
			if($stmt->execute()) {
				$insertedId = $this->pdo->lastInsertId();
				return $this->selectById($insertedId);
			}
		}
		return false;
	}

	public function delete($id) {
		$sql = "DELETE FROM `yow_orders` WHERE `id` = :id";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(':id', $id);
		return $stmt->execute();
	}

	public function getValidationErrors($data) {
		$errors = array();
		if(!isset($data['user_id'])) {
			$errors['user_id'] = "Please fill in a user_id";
		}
		if(!isset($data['date'])) {
			$errors['date'] = "Please fill in a date";
		}
		return $errors;
	}

}