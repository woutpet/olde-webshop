<?php
require_once __DIR__ . '/DAO.php';
class OrderProductDAO extends DAO {

	public function selectAll() {
		$sql = "SELECT * FROM `yow_orders_products`";
		$stmt = $this->pdo->prepare($sql);
		$stmt->execute();
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}

	public function selectById($id) {
		$sql = "SELECT * FROM `yow_orders_products` WHERE `id` = :id";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(':id', $id);
		$stmt->execute();
		return $stmt->fetch(PDO::FETCH_ASSOC);
	}

	public function selectByOrderId($id){
		$sql = "SELECT *
				FROM `yow_orders_products` 
				INNER JOIN `yow_orders` ON `yow_orders_products`.`order_id` = `yow_orders`.`id`
				INNER JOIN `yow_products` ON `yow_orders_products`.`product_id` = `yow_products`.`id`
				WHERE `order_id` = :id;";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(':id', $id);
		$stmt->execute();
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}

	// public function selectById($postId) {
	// 	$sql = "SELECT * FROM `yow_orders_products` WHERE `review_id` = :review_id";
	// 	$stmt = $this->pdo->prepare($sql);
	// 	$stmt->bindValue(':review_id', $postId);
	// 	$stmt->execute();
	// 	return $stmt->fetchAll(PDO::FETCH_ASSOC);
	// }

	// public function insert($data) {
	// 	$errors = $this->getValidationErrors($data);
	// 	if(empty($errors)) {
	// 		$sql = "INSERT INTO `yow_orders_products` (`user_id`, `date`) VALUES (:user_id, :date)";
	// 		$stmt = $this->pdo->prepare($sql);
	// 		$stmt->bindValue(':user_id', $data['user_id']);
	// 		$stmt->bindValue(':date', $data['date']);
	// 		if($stmt->execute()) {
	// 			$insertedId = $this->pdo->lastInsertId();
	// 			return $this->selectById($insertedId);
	// 		}
	// 	}
	// 	return false;
	// }

	public function insertMultiple($data) {
		//$errors = $this->getValidationErrors($data);
		//if(empty($errors)) {
		$sql = "INSERT INTO `yow_orders_products` (`order_id`, `product_id`, `amount`) VALUES ";
		foreach ($data as $product) {
			$id = $product['product_id'] . '_';
			$sql .= "(:" . $id . "order_id, :" . $id . "product_id, :" . $id . "amount), ";
		}
		//$sql = substr($sql, 0,strlen($sql)-1);
		
		//var_dump($data);

		$sql = substr($sql, 0, -2);
		$stmt = $this->pdo->prepare($sql);
		foreach ($data as $product) {
			$id = $product['product_id'] . '_';
			$stmt->bindValue(":" . $id . "order_id", $product['order_id']);
			$stmt->bindValue(":" . $id . "product_id", $product['product_id']);
			$stmt->bindValue(":" . $id . "amount", $product['amount']);
		}

		if ($stmt->execute()) {
			return true;
		}
		return false;
	}

	public function delete($id) {
		$sql = "DELETE FROM `yow_orders_products` WHERE `id` = :id";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(':id', $id);
		return $stmt->execute();
	}

	public function getValidationErrors($data) {
		$errors = array();

		foreach ($data as $product) {
			if(!isset($product['order_id'])) {
				$errors[$product["name"] . "order_id"] = "Please fill in an order_id";
			}
			if(!isset($product['id'])) {
				$errors[$product["id"] . "product_id"] = "Please fill in a product_id";
			}
			if(!isset($product['amount'])) {
				$errors[$product["name"] . "amount"] = "Please fill in an amount";
			}
		}

		return $errors;
	}

}