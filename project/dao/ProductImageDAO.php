<?php
require_once WWW_ROOT . 'dao' . DS . 'DAO.php';
class ProductImageDAO extends DAO {

	public function selectByProductId($product_id) {
		$sql = "SELECT * FROM `product_images` WHERE `product_id` = :product_id";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(':product_id', $product_id);
		$stmt->execute();
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function selectAll() {
		$sql = "SELECT * FROM `product_images`";
		$stmt = $this->pdo->prepare($sql);
		$stmt->execute();
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}

	public function selectById($id) {
		$sql = "SELECT * FROM `product_images` WHERE `id` = :id";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(':id', $id);
		$stmt->execute();
		return $stmt->fetch(PDO::FETCH_ASSOC);
	}

	public function insert($data) {
		$errors = $this->getValidationErrors($data);
		if(empty($errors)) {
			$sql = "INSERT INTO `product_images` (`product_id`, `image`) VALUES (:product_id, :image)";
			$stmt = $this->pdo->prepare($sql);
			$stmt->bindValue(':product_id', $data['product_id']);
			$stmt->bindValue(':image', $data['image']);
			if($stmt->execute()) {
				$insertedId = $this->pdo->lastInsertId();
				return $this->selectById($insertedId);
			}
		}
		return false;
	}

	public function update($id, $data) {
		$errors = $this->getValidationErrors($data);
		if(empty($errors)) {
			$sql = "UPDATE `product_images` SET `product_id` = :product_id, `image` = :image WHERE `id` = :id";
			$stmt = $this->pdo->prepare($sql);
			$stmt->bindValue(':product_id', $data['product_id']);
			$stmt->bindValue(':image', $data['image']);
			$stmt->bindValue(':id', $id);
			if($stmt->execute()) {
				return $this->selectById($id);
			}
		}
		return false;
	}

	public function delete($id) {
		$sql = "DELETE FROM `product_images` WHERE `id` = :id";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(':id', $id);
		return $stmt->execute();
	}

	public function getValidationErrors($data) {
		$errors = array();
		if(!isset($data['product_id'])) {
			$errors['product_id'] = 'Please Enter a Product Id';
		}
		if(!isset($data['image'])) {
			$errors['image'] = 'Please Enter an Image';
		}
		return $errors;
	}
}