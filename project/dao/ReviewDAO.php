<?php
require_once __DIR__ . '/DAO.php';
class ReviewDAO extends DAO {

	public function selectAll() {
		$sql = "SELECT * FROM `yow_reviews`";
		$stmt = $this->pdo->prepare($sql);
		$stmt->execute();
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}

	public function selectById($id) {
		$sql = "SELECT * FROM `yow_reviews` WHERE `id` = :id";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(':id', $id);
		$stmt->execute();
		return $stmt->fetch(PDO::FETCH_ASSOC);
	}

	public function selectByProductId($id) {
		$sql = "SELECT `yow_reviews`.*, `yow_users`.`email`
				FROM `yow_reviews`
				INNER JOIN `yow_users`
				ON `yow_reviews`.`user_id` = `yow_users`.`id`
				WHERE `product_id` = :product_id";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(':product_id', $id);
		$stmt->execute();
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}

	public function insert($data) {
		$errors = $this->getValidationErrors($data);
		if(empty($errors)) {
			$sql = "INSERT INTO `yow_reviews` (`title`, `user_id`, `content`, `product_id`, `creation_date`) VALUES (:title, :user_id, :content, :product_id, :creation_date)";
			$stmt = $this->pdo->prepare($sql);
			$stmt->bindValue(':title', $data['title']);
			$stmt->bindValue(':user_id', $data['user_id']);
			$stmt->bindValue(':content', $data['content']);
			$stmt->bindValue(':product_id', $data['product_id']);
			$stmt->bindValue(':creation_date', $data['creation_date']);
			if($stmt->execute()) {
				$insertedId = $this->pdo->lastInsertId();
				return $this->selectById($insertedId);
			}
		}
		return false;
	}

	public function delete($id) {
		$sql = "DELETE FROM `yow_reviews` WHERE `id` = :id";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(':id', $id);
		return $stmt->execute();
	}

	public function getValidationErrors($data) {
		$errors = array();
		if(!isset($data['title'])) {
			$errors['title'] = "Please fill in a title";
		}
		if(!isset($data['content'])) {
			$errors['content'] = "Please fill in the content";
		}
		if(empty($data['user_id'])) {
			$errors['user_id'] = "Please fill in a user_id";
		}
		if(!isset($data['product_id'])) {
			$errors['product_id'] = "Please fill in a product_id";
		}
		if(!isset($data['creation_date'])) {
			$errors['creation_date'] = "Please fill in a creation_date";
		}
		return $errors;
	}

}