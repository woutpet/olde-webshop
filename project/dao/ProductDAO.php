<?php
require_once __DIR__ . '/DAO.php';
class ProductDAO extends DAO {

	public function selectAll() {
		$sql = "SELECT * FROM `yow_products`";
		$stmt = $this->pdo->prepare($sql);
		$stmt->execute();
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}

	public function selectById($id) {
		$sql = "SELECT * FROM `yow_products` WHERE `id` = :id";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(':id', $id);
		$stmt->execute();
		return $stmt->fetch(PDO::FETCH_ASSOC);
	}

	public function searchProducts($searchString) {
		$sql = "SELECT * FROM `yow_products` WHERE `title` LIKE :title LIMIT 0, 100";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(':title', '%' . $searchString . '%');
		$stmt->execute();
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}

	// public function selectByPostId($postId) {
	// 	$sql = "SELECT * FROM `yow_products` WHERE `post_id` = :post_id";
	// 	$stmt = $this->pdo->prepare($sql);
	// 	$stmt->bindValue(':post_id', $postId);
	// 	$stmt->execute();
	// 	return $stmt->fetchAll(PDO::FETCH_ASSOC);
	// }

	public function insert($data) {
		$errors = $this->getValidationErrors($data);
		if(empty($errors)) {
			$sql = "INSERT INTO `yow_products` (`title`, `image`, `content`, `price`, `stock`, `sold`) VALUES (:title, :image, :content, :price, :stock, :sold)";
			$stmt = $this->pdo->prepare($sql);
			$stmt->bindValue(':title', $data['title']);
			$stmt->bindValue(':image', $data['image']);
			$stmt->bindValue(':content', $data['content']);
			$stmt->bindValue(':price', $data['price']);
			$stmt->bindValue(':stock', $data['stock']);
			$stmt->bindValue(':sold', $data['sold']);
			if($stmt->execute()) {
				$insertedId = $this->pdo->lastInsertId();
				return $this->selectById($insertedId);
			}
		}
		return false;
	}

	public function update($data){

		$sql = "UPDATE `yow_products`
				SET `stock`=:stock,`sold`=:sold
				WHERE id=:id";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(':stock', $data['stock']);
		$stmt->bindValue(':sold', $data['sold']);
		$stmt->bindValue(':id', $data['id']);

		return $stmt->execute();

	}	

	public function delete($id) {
		$sql = "DELETE FROM `yow_products` WHERE `id` = :id";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(':id', $id);
		return $stmt->execute();
	}

	public function getValidationErrors($data) {
		$errors = array();
		if(!isset($data['title'])) {
			$errors['title'] = "Please fill in a title";
		}
		if(!isset($data['image'])) {
			$errors['image'] = "Please fill in an image";
		}
		if(empty($data['content'])) {
			$errors['content'] = "Please fill in the content";
		}
		if(!isset($data['price'])) {
			$errors['price'] = "Please fill in a price";
		}
		if(!isset($data['stock'])) {
			$errors['stock'] = "Please fill in stock";
		}
		if(!isset($data['sold'])) {
			$errors['sold'] = "Please fill in sold";
		}
		return $errors;
	}

}