(function(){

	var searchForm, searchInput;

	function init() {
		searchForm = document.getElementById('searchForm');
		if(searchForm) {
			initSearchForm();
		}
	}

	function initSearchForm() {
		searchInput = searchForm.querySelector('input[name=q]');
		searchForm.addEventListener('submit', submitSearchHandler);
		searchInput.addEventListener('input', submitSearchHandler);
	}

	function submitSearchHandler(event) {
		event.preventDefault();
		var req = new XMLHttpRequest();
		req.onload = function() {
			var responseDiv = document.createElement('div');
			responseDiv.innerHTML = req.responseText;
			var responseResult = responseDiv.querySelector('.product-grid');
			var origResult = document.querySelector('.product-grid');
			origResult.parentNode.replaceChild(responseResult, origResult);
		};
		req.open('get', searchForm.getAttribute('action') + '&q=' + searchInput.value, true);
		req.setRequestHeader('X_REQUESTED_WITH', 'xmlhttprequest');
		req.send();
	}

	init();
})();